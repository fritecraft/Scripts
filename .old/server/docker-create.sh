#!/bin/bash

docker run -d \
-v /mnt/data/proxy:/server \
-p 25565:25577 \
-e MEMORY=512m \
-e INIT_MEMORY=512m \
-e MAX_MEMORY=512m \
-e BUNGEE_BASE_URL=https://papermc.io/ci/job/Waterfall \
-e BUNGEE_JOB_ID=lastStableBuild \
-e BUNGEE_JAR_URL=https://papermc.io/ci/job/Waterfall/lastSuccessfulBuild/artifact/Waterfall-Proxy/bootstrap/target/Waterfall.jar \
--restart=always \
--name mc_proxy itzg/bungeecord

docker run -d \
-v /mnt/data/hub:/data \
-e ONLINE_MODE=FALSE \
-e EULA=TRUE \
-e MEMORY=1G \
-e INIT_MEMORY=1G \
-e MAX_MEMORY=1G \
-e TYPE=PAPER \
-e VERSION=1.15.2 \
-p 25580:25565 \
--restart=always \
--name mc_hub itzg/minecraft-server --noconsole
