#! /bin/sh
# /etc/init.d/ramdisk
### BEGIN INIT INFO
# Provides: ramdisk
# Required-Start: $local_fs $remote_fs $syslog $named $network $time
# Required-Stop: $local_fs $remote_fs $syslog $named $network
# Should-Start:
# Should-Stop:
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: ramdisk sync for files
# Description: ramdisk syncing of files
### END INIT INFO

start=`date +%s`
FILE=/mnt/ramdisk/file

case "$1" in
 start)
#        ramdisk=$(du -sh /mnt/ramdisk | head -c2)
#        echo "${ramdisk}"
#        if [ $ramdisk -lt 1 ]; then
        if [ ! -f "$FILE" ]; then
          echo "$FILE does not exist."
          echo "Copie du disque vers RAM : "
          rsync -av /home/minecraft/servers/ /mnt/ramdisk/servers
          rsync -av /home/minecraft/volumes/ /mnt/ramdisk/volumes
          touch /mnt/ramdisk/file
          if [ -f "$FILE" ]; then
            echo "$FILE created." >> /var/log/ramdisk_sync.log
          fi
          echo [`date +"%Y-%m-%d %H:%M"`] Ramdisk initialisé >> /var/log/ramdisk_sync.log
        elif [ -f "$FILE" ]; then
          echo "$FILE exists."
          echo "Ramdisk semble contenir des fichiers ..."
        fi
        ;;
 sync)
#        ramdisk=$(du -sh /mnt/ramdisk | head -c2)
#        echo "${ramdisk}"
        if [ -f "$FILE" ]; then
          echo "$FILE exists."
          echo "Copie RAM vers disque : "
          rsync -av --delete --recursive --force /mnt/ramdisk/servers/ /home/minecraft/servers/ && rsync -av --delete --recursive --force /mnt/ramdisk/volumes/ /home/minecraft/volumes/ && curl -fsS -m 10 --retry 5 -o /dev/null https://hc-ping.com/c955dca9-d032-449c-b5ba-d27debdc3a62
          echo [`date +"%Y-%m-%d %H:%M"`] Ramdisk sauvegardé >> /var/log/ramdisk_sync.log
          echo `date +%s` > /var/log/ramdisk
        elif [ ! -f "$FILE" ]; then
          echo "$FILE does not exist."
          echo "Ramdisk semble vide ..."
        fi

        ;;
 *)
        echo "Usage: /etc/init.d/ramdisk {start|sync}"
        exit 1
        ;;
esac

end=`date +%s`
runtime=$( echo "$end - $start" | bc -l )
echo [`date +"%Y-%m-%d %H:%M"`] $runtime  >> /var/log/ramdisk_sync.log

exit 0
