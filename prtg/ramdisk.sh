#!/bin/bash

now=`date +%s`
last=`cat /var/log/ramdisk`

since=$( echo "$now - $last" | bc -l )
since=$(expr $since / 60 )
#echo $since


if [ $since -lt 4000 ]
then
        echo "0:$since:$since minutes depuis sync"
fi

if [ $since -gt 4000 ]
then
        echo "1:$since:$since minutes depuis sync"
fi
