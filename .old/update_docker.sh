#!/bin/bash
docker stop mc_proxy
docker stop mc_ressources
docker stop mc_hub
docker stop mc_bouquenia
docker stop mc_survie
docker stop mc_aventure

# Pull les images docker
docker pull itzg/bungeecord
docker pull itzg/minecraft-server
sleep 1

# Update Ressources
docker rm mc_ressources
docker create \
-v /mnt/data/ressources:/data \
-v /mnt/data/_commondata:/mnt/data/_commondata \
-e ONLINE_MODE=FALSE \
-e EULA=TRUE \
-e USE_AIKAR_FLAGS=true \
-e MEMORY=10G \
-e INIT_MEMORY=10G \
-e MAX_MEMORY=10G \
-e TYPE=PAPER \
-e VERSION=LATEST \
-p 25590:25565 \
--restart=unless-stopped \
--name mc_ressources itzg/minecraft-server --noconsole
sleep 1

# Update Aventure
docker rm mc_aventure
docker create \
-v /mnt/data/aventure:/data \
-v /mnt/data/_commondata:/mnt/data/_commondata \
-e ONLINE_MODE=FALSE \
-e EULA=TRUE \
-e USE_AIKAR_FLAGS=true \
-e MEMORY=10G \
-e INIT_MEMORY=10G \
-e MAX_MEMORY=10G \
-e TYPE=PAPER \
-e VERSION=LATEST \
-p 25583:25565 \
--restart=unless-stopped \
--name mc_aventure itzg/minecraft-server --noconsole
sleep 1

# Update Bouquenia
docker rm mc_bouquenia
docker create \
-v /mnt/data/bouquenia:/data \
-v /mnt/data/_commondata:/mnt/data/_commondata \
-e ONLINE_MODE=FALSE \
-e EULA=TRUE \
-e USE_AIKAR_FLAGS=true \
-e MEMORY=10G \
-e INIT_MEMORY=10G \
-e MAX_MEMORY=10G \
-e TYPE=PAPER \
-e VERSION=LATEST \
-p 25581:25565 \
--restart=unless-stopped \
--name mc_bouquenia itzg/minecraft-server --noconsole
sleep 1

# Update Hub
docker rm mc_hub
docker create \
-v /mnt/data/hub:/data \
-v /mnt/data/_commondata:/mnt/data/_commondata \
-e ONLINE_MODE=FALSE \
-e EULA=TRUE \
-e USE_AIKAR_FLAGS=true \
-e MEMORY=6G \
-e INIT_MEMORY=6G \
-e MAX_MEMORY=6G \
-e TYPE=PAPER \
-e VERSION=LATEST \
-p 25580:25565 \
--restart=unless-stopped \
--name mc_hub itzg/minecraft-server --noconsole
sleep 1

# Update Survie
docker rm mc_survie
docker create \
-v /mnt/data/pitman:/data \
-v /mnt/data/_commondata:/mnt/data/_commondata \
-e ONLINE_MODE=FALSE \
-e EULA=TRUE \
-e USE_AIKAR_FLAGS=true \
-e MEMORY=10G \
-e INIT_MEMORY=10G \
-e MAX_MEMORY=10G \
-e TYPE=PAPER \
-e VERSION=LATEST \
-p 25582:25565 \
--restart=unless-stopped \
--name mc_survie itzg/minecraft-server --noconsole
sleep 1

docker run -d \
-v /mnt/data/potos:/data \
-v /mnt/data/_commondata:/mnt/data/_commondata \
-e ONLINE_MODE=TRUE \
-e EULA=TRUE \
-e USE_AIKAR_FLAGS=true \
-e MEMORY=10G \
-e INIT_MEMORY=10G \
-e MAX_MEMORY=10G \
-e TYPE=PAPER \
-e VERSION=LATEST \
-p 25599:25565 \
--restart=always \
--name mc_vanilla itzg/minecraft-server --noconsole
