
#!/bin/bash

export PATH=/bin:/usr/bin:/usr/local/bin
TODAY=`date +"%Y-%B-%d"`
INDEX_DAY=`date +"%j"`
HOURLY=`date +"%Y-%B-%d-%H"`
INDEX_HOUR=`date +"%H"`

source "$(dirname -- "$0")/config.sh"

#################################################################

mkdir -p ${BACKUP_PATH}

for i in $*
do
	echo -e "sauvegarde de la base ${i}"
	mysqldump -h ${MYSQL_HOST} -u ${MYSQL_USERNAME} -p${MYSQL_PASSWORD} --verbose ${i} > ${BACKUP_PATH}/${INDEX_DAY}-${i}-${TODAY}.sql
	if test $? -eq 0
	then
 		echo "Sauvegarde OK"
	else
		echo "Erreur pendant la sauvegarde"
	fi
done
