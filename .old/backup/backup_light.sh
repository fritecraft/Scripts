#!/bin/bash

###################################################
### VARIABLES
###################################################

  date=$(date +"%y-%m-%d")
  res1=$(date +%s.%N)

#### Login MySQL
  USERNAME=$(cat /root/username)
  PASSWORD=$(cat /root/password)

#### Couleurs
  White='\e[1;37m'
  Blue='\e[0;34m'
  Green='\e[0;32m'
  Cyan='\e[0;36m'
  Red='\e[0;31m'
  Purple='\e[0;35m'
  Yellow='\e[1;33m'
  Grey='\e[0;30m'
  NC='\e[39m'


#### Chemins
  path_backup_server="/home/minecraft/backups/server"
  path_backup_worlds="/home/minecraft/backups/worlds"
  path_backup_mysql="/home/minecraft/backups/mysql"
  path_server="/home/minecraft/fritecraft"


  ###################################################
  ### FONCTIONS
  ###################################################

script_timings()
{
  res2=$(date +%s.%N)
  dt=$(echo "$res2 - $res1" | bc)
  dd=$(echo "$dt/86400" | bc)
  dt2=$(echo "$dt-86400*$dd" | bc)
  dh=$(echo "$dt2/3600" | bc)
  dt3=$(echo "$dt2-3600*$dh" | bc)
  dm=$(echo "$dt3/60" | bc)
  ds=$(echo "$dt3-60*$dm" | bc)
  printf "Temps écoulé: %02.4f s \n" $ds
}


script_start(){
  echo -e "${Green}--------------------------------------------------------------------------------${NC}"
  echo -e "${Green}------------------------   DEBUT  DU  SCRIPT  ----------------------------------${NC}"
  echo -e "${Green}--------------------------------------------------------------------------------${NC}"
  script_timings
}

script_end(){
  script_timings
  echo -e "${Red}--------------------------------------------------------------------------------${NC}"
  echo -e "${Red}--------------------------   FIN   DU   SCRIPT  --------------------------------${NC}"
  echo -e "${Red}--------------------------------------------------------------------------------${NC}"
}

backup_rotate()
{
  cd $path_backup_server
  rm -f $(ls -1t . | tail -n +8)
  cd $path_backup_worlds
  rm -f $(ls -1t . | tail -n +8)
  cd $path_backup_mysql
  rm -f $(ls -1t . | tail -n +8)
}

backup_diskcheck()
{
  DISQUE=$(df --output=avail / | grep [0-9])
  DISQUE=$(echo "${DISQUE::-6}")
  if [ $DISQUE -gt 20 ]
  then
    python3 /root/discord.py ":information_source: $DISQUE Go de libre"
  else
    python3 /root/discord.py ":warning: **ESPACE INSUFFISANT**"
    exit 1
  fi
}

backup_annonce()
{
  echo -e "${PURPLE}Sauvegarde du serveur !${NC}"
  python3 /root/discord.py ":information_source: Sauvegarde du serveur en cours... :hourglass:"
}

backup_plugins()
{
  cd $path_server

  echo -e "${BLUE} Compression des plugins en cours... ${NC}"

  tar -czvf $path_backup_server/backup_server-"$date.tar.gz" \
  ./plugins/ \
  ./data-storage/ \
  ./crash reports/ \
  ./timings/ \
  --exclude 'tiles/*' \

  echo -e "${Purple} Serveur sauvegardé !${NC}"
#  ARCHIVE=$(du -h /home/minecraft/backups/*.gz -a | cut -d / -f1 | tail -n 1)
  sleep 1
  script_timings
}


backup_mysql()
{
  echo -e "${BLUE} Sauvegarde de la base de données en cours... ${NC}"
  mysqldump --all-databases > $path_backup_mysql/backup_base-$date.sql -u $USERNAME -p$PASSWORD
  echo -e "${Purple} Base de données sauvegardée !${NC}"
#  BDD=$(du -h /home/minecraft/backups/*.sql -a | cut -d / -f1 | tail -n 1)
  sleep 1
  script_timings
}

backup_bouquenia()
{
  echo -e "${BLUE} Compression des mondes en cours... ${NC}"

  tar -czvf $path_backup_worlds/backup_bouquenia-"$date.tar.gz" ./Bouquenia/
  ARCHIVE=$(du -h /home/minecraft/backups/*.gz -a | cut -d / -f1 | tail -n 1)
  python3 /root/discord.py ":white_check_mark: backup_bouquenia-$date.tar.gz      $ARCHIVE"
  sleep 1
  script_timings
}

backup_nether()
{
  tar -czvf $path_backup_worlds/backup_bouquenia_nether-"$date.tar.gz" ./Bouquenia_nether/
  ARCHIVE=$(du -h /home/minecraft/backups/*.gz -a | cut -d / -f1 | tail -n 1)
  python3 /root/discord.py ":white_check_mark: backup_bouquenia_nether-$date.tar.gz      $ARCHIVE"
  sleep 1
  script_timings
}

backup_end()
{
  tar -czvf $path_backup_worlds/backup_bouquenia_the_end-"$date.tar.gz" ./Bouquenia_the_end/
  ARCHIVE=$(du -h /home/minecraft/backups/*.gz -a | cut -d / -f1 | tail -n 1)
  python3 /root/discord.py ":white_check_mark: backup_bouquenia_the_end-$date.tar.gz      $ARCHIVE"
  sleep 1
}

backup_pitman()
{
  tar -czvf $path_backup_worlds/backup_pitman-"$date.tar.gz" ./Pitman/
  ARCHIVE=$(du -h /home/minecraft/backups/*.gz -a | cut -d / -f1 | tail -n 1)
  python3 /root/discord.py ":white_check_mark: backup_pitman-$date.tar.gz      $ARCHIVE"
  sleep 1
  script_timings
}

backup_hub()
{
  tar -czvf $path_backup_worlds/backup_HUB-"$date.tar.gz" ./HUB/
  ARCHIVE=$(du -h /home/minecraft/backups/*.gz -a | cut -d / -f1 | tail -n 1)
  python3 /root/discord.py ":white_check_mark: backup_hub-$date.tar.gz      $ARCHIVE"
  sleep 1
  script_timings
}


######################################################################################################
######################################################################################################
#####                                                                                            #####
#####         LE SCRIPT PRINCIPAL COMMENCE ICI                                                   #####
#####                                                                                            #####
######################################################################################################
######################################################################################################

script_start >> $path_server/script.log
backup_annonce >> $path_server/script.log

backup_diskcheck >> $path_server/script.log

backup_plugins >> $path_server/script.log
backup_mysql >> $path_server/script.log

backup_bouquenia >> $path_server/script.log
backup_pitman >> $path_server/script.log

script_end >> $path_server/script.log
