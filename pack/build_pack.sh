#!/bin/bash

date=$(date +"%y-%m-%d")
echo -e "Date : ${date}"

mkdir -p /tmp/items
cd /tmp/items

## Synchronisation du pack avec github
git clone https://gitlab.com/fritecraft/Items .
cd items
cd Fritecraft_Items
git pull

# Compresser en .zip
zip -r /mnt/data/Fritecraft_Items.zip *
cd
rm -rf /tmp/items

# Calculer SHA1
#cd ..
#sha1sum Fritecraft_Items.zip | sed -r 's/.{22}$//' > items.sha1
#SHA1=$(cat items.sha1)
#echo -e "SHA1 : ${SHA1}"

#mv Fritecraft_Items.zip Fritecraft_Items-$SHA1.zip

#rm items.sha1





# Sauvegarde du fichier server.properties
# echo -e "Sauvegarde de server.properties ..."
# cp ../../fritecraft/server.properties ../../backups/properties/server.properties-$date

# Ajouter SHA1 dans server.properties
#echo -e "Suprression de la ligne 'resource-pack-sha1' ..."
#sed -i '/resource-pack-sha1=/d' ../../fritecraft/server.properties
#echo -e "Ajout de resource-pack-sha1=${SHA1}"
#echo -e "resource-pack-sha1=$SHA1" >> ../../fritecraft/server.properties

# Export pour téléchargement
#echo -e "Upload du pack sur B2 ..."
#b2 upload-file FRITECRAFT ./Fritecraft_Items.zip Fritecraft_Items.zip
#echo -e "Génération du lien de téléchargement B2 ..."
#b2 get-download-url-with-auth Ventouz-frite Fritecraft_Items.zip > download.b2
#DOWNLOAD=$(cat download.b2)
#echo -e "Lien de téléchargement : ${DOWNLOAD}"

## Ajout du lien de téléchargement au server.properties
#echo -e "Suprression de la ligne 'resource-pack' ..."
#sed -i '/resource-pack=/d' ../../fritecraft/server.properties
#echo -e "Ajout de resource-pack=${DOWNLOAD}"
#echo -e "resource-pack=$DOWNLOAD" >> ../../fritecraft/server.properties
