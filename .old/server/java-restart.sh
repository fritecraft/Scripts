#!/bin/bash

## Pour la Crontab :
## * 0-4,6-23 * * * /root/FritecraftScripts/restart.sh

date=$(date +"%y-%m-%d")

if ! screen -list | grep -q "minecraft"; then
    echo "serveur actif"
fi
    echo -e "${date} SERVEUR HORS LIGNE" >> /var/log/cron/restart.log
    sleep 10
#    ./start.sh
    python3 /root/discord.py ":hourglass_flowing_sand:  **Le serveur est en train de redémarrer ...**"
