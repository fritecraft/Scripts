#!/bin/bash


#Couleur rouge
RED='\033[0;31m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'

#Pas de couleurs
NC='\033[0m'

#Configuration
backup_path="/home/minecraft/backups"
date=$(date +"%d-%m-%y")

# Afficher un texte, en couleur et retirer la couleur
echo -e "${PURPLE}arrêt du serveur !${NC}"


#Stopper le serveur en lui laissant deux minutes
screen -S minecraft -p 0 -X stuff 'stop 
'
