#!/bin/bash

#Relancer le serveur
echo -e "${Purple} Démarrage du serveur... ${NC}"
cd /home/minecraft/fritecraft && screen -dmS minecraft java -Xms5G -Xmx5G -XX:+UseG1GC -XX:MaxGCPauseMillis=50 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:TargetSurvivorRatio=90 -XX:G1NewSizePercent=50 -XX:G1MaxNewSizePercent=80 -XX:InitiatingHeapOccupancyPercent=10 -XX:G1MixedGCLiveThresholdPercent=50 -XX:+AggressiveOpts -XX:+AlwaysPreTouch -jar /home/minecraft/fritecraft/paper.jar nogui
