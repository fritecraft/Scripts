#!/bin/bash
cd /tmp
rm plugins

docker ps --format '{{.Names}}' > container_list
sed -i '/proxy/d' container_list

cat container_list | while read line
do
   echo "${line} :" >> plugins
   docker exec $line rcon-cli pl > line
   sed -i 's/Plugins (..)://' line
   sed -i 's/Plugins (.)://' line
   tr , '\n' < line > tmp
   cat tmp > line
   rm tmp
   sed -i '/§a/d' line
   sed -i 's/§a//g' line
   sed -i 's/§c//g' line
   sed -i 's/§f//g' line
   cat line >> plugins
   rm line
done
rm container_list
errors=$(tr -cd '!' < /tmp/plugins | wc -c)
if [ $errors -ne 0 ]
then
        echo "1:$errors:$errors plugins en erreur"
fi

if [ $errors -eq 0 ]
then
        echo "0:$errors:OK"
fi
