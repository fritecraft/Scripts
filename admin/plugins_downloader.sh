#!/bin/bash

LIBRARY="/mnt/minecraft/library/"
TODAY=`date +"%Y-%B-%d"`
INDEX_DAY=`date +"%j"`

cd /tmp

# Citizens
wget https://ci.citizensnpcs.co/job/Citizens2/lastSuccessfulBuild/artifact/*zip*/archive.zip -O Citizens-${INDEX_DAY}.zip
unzip Citizens-${INDEX_DAY}.zip -d Citizens
rm -v Citizens-${INDEX_DAY}.zip
mv -v Citizens/archive/dist/target/Citizens*.jar ${LIBRARY}
ls -tr1 ${LIBRARY}Citizens*.jar | head -n -1 | xargs --no-run-if-empty rm

# LibsDisguise
wget https://ci.md-5.net/job/LibsDisguises/lastSuccessfulBuild/artifact/target/LibsDisguises.jar -O LibsDisguise-${INDEX_DAY}.jar
mv -v LibsDisguise-${INDEX_DAY}.jar ${LIBRARY}
ls -tr1 ${LIBRARY}LibsDisguise*.jar | head -n -1 | xargs --no-run-if-empty rm

# ProtocolLib
wget https://ci.dmulloy2.net/job/ProtocolLib/lastSuccessfulBuild/artifact/target/ProtocolLib.jar -O ProtocolLib-${INDEX_DAY}.jar
mv -v ProtocolLib-${INDEX_DAY}.jar ${LIBRARY}
ls -tr1 ${LIBRARY}ProtocolLib*.jar | head -n -1 | xargs --no-run-if-empty rm

# Luckperms
wget https://ci.lucko.me/job/LuckPerms/lastSuccessfulBuild/artifact/*zip*/archive.zip -O LuckPerms-${INDEX_DAY}.zip
unzip LuckPerms-${INDEX_DAY}.zip -d LuckPerms
rm -rv LuckPerms-${INDEX_DAY}.zip
mv -v LuckPerms/archive/bukkit/loader/build/libs/LuckPerms-Bukkit*.jar ${LIBRARY}
mv -v LuckPerms/archive/bungee/loader/build/libs/LuckPerms-Bungee*.jar ${LIBRARY}
ls -tr1 ${LIBRARY}LuckPerms-Bukkit*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 ${LIBRARY}LuckPerms-Bungee-*.jar | head -n -1 | xargs --no-run-if-empty rm

