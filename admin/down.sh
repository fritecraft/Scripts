#!/bin/bash
cd /scripts/

docker-compose stop proxy
sleep 10
docker-compose stop hub
sleep 10
docker-compose stop bouquenia
sleep 10
docker-compose stop survie
sleep 10
docker-compose stop ressources
sleep 10
docker-compose stop test
sleep 10
