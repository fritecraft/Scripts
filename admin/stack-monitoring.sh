#!/bin/bash

# Cadvisor
docker run --name=cadvisor -d \
-v /:/rootfs:ro -v /run:/var/run:rw -v /sys:/sys:ro -v /var/lib/docker:/var/lib/docker:ro \
--network fritecraft \
-p 8080:8080 \
--restart always \
gcr.io/cadvisor/cadvisor

# Prometheus
docker run --name=prometheus -d \
-v /scripts/prometheus:/etc/prometheus/ \
-v scripts_prometheus_data:/prometheus \
--network fritecraft \
-p 9090:9090 \
--restart always \
prom/prometheus:v2.1.0 \
--config.file=/etc/prometheus/prometheus.yml \
--storage.tsdb.path=/prometheus \
--web.console.libraries=/usr/share/prometheus/console_libraries \
--web.console.templates=/usr/share/prometheus/consoles

# Grafana
docker run --name=grafana -d \
--env-file /scripts/grafana/config.monitoring \
-v scripts_grafana_data:/var/lib/grafana \
-v /scripts/grafana/provisioning:/etc/grafana/provisioning/ \
--network fritecraft \
-p 3000:3000 -u 472 \
--restart always \
grafana/grafana

#Node-exporter
docker run --name=node-exporter -d \
-v /proc:/host/proc:ro -v /sys:/host/sys:ro -v /:/rootfs:ro \
--network fritecraft \
-p 9100:9100 \
--restart always \
prom/node-exporter \
--path.procfs=/host/proc \
--path.sysfs=/host/sys 

# Alertmanager
docker run --name=alertmanager -d \
-v /scripts/alertmanager:/etc/alertmanager/ \
--network fritecraft \
-p 9093:9093 \
--restart always \
prom/alertmanager \
--config.file=/etc/alertmanager/config.yml \
--storage.path=/alertmanager





